import React from "react";
import { connect } from "react-redux";

function Home(props: any) {
  return (
    <div className={props.themee.darkMode ? "home darkMode" : "home"}>
      <h1 className={props.themee.darkMode ? "h1 darkModeFont" : "h1"}>
        Hello There, welcome on my rwd website
      </h1>
      <h1
        className={props.themee.darkMode ? "h1 right darkModeFont" : "h1 right"}
      >
        {" "}
        This is my first try doing somthing that doesn't static {" "}
      </h1>
      <h1 className={props.themee.darkMode ? "h1 darkModeFont" : "h1"}>
        So here i'm, to match colors i used coolors.co{" "}
      </h1>
      <h1
        className={props.themee.darkMode ? "h1 right darkModeFont" : "h1 right"}
      >
        This app Only contains text and some sort of simple animations{" "}
      </h1>
      <h1 className={props.themee.darkMode ? "h1 darkModeFont" : "h1"}>
        {" "}
        There is button that activates dark mode in bottom of menu on the left side
      </h1>
    </div>
  );
}
const mapStateToProps = (state: any) => ({
  themee: state.theme,
});

export default connect(mapStateToProps, null)(Home);
