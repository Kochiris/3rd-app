import React from "react";
import { connect } from "react-redux";

function GitLab(props: any) {
  return (
    <div className={props.themee.darkMode ? "gitlab darkMode" : "gitlab"}>
      <a
        className={
          props.themee.darkMode ? "gitlab-link darkModeFont" : "gitlab-link"
        }
        href="https://gitlab.com/users/Kochiris/projects"
      >
        My GitLab projects
      </a>
    </div>
  );
}
const mapStateToProps = (state: any) => ({
  themee: state.theme,
});

export default connect(mapStateToProps, null)(GitLab);
