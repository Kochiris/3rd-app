import React from "react";
import { connect } from "react-redux";

function AboutMe(props: any) {
  return (
    <div className={props.themee.darkMode ? "aboutme darkMode" : "aboutme"}>
      <h1 className={props.themee.darkMode ? "h1 darkModeFont" : "h1"}>
        I'm first year student at Politechnika Wrocławska
      </h1>
      <h1
        className={props.themee.darkMode ? "h1 right darkModeFont" : "h1 right"}
      >
        {" "}
        My hobby is Japanese carpentry and i love spending as much time as i can
        on it{" "}
      </h1>
      <h1 className={props.themee.darkMode ? "h1 darkModeFont" : "h1"}>
        I also have started to study AI and Machine learning by my own {" "}
      </h1>
    </div>
  );
}
const mapStateToProps = (state: any) => ({
  themee: state.theme,
});

export default connect(mapStateToProps, null)(AboutMe);
