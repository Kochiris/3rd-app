import React from "react";
import "./App.css";
import NavBar from "./components/navBar/NavBar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import AboutMe from "./pages/AboutMe";
import GitLab from "./pages/GitLab";

function App() {
  return (
    <>
      <Router>
        <NavBar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/aboutme" component={AboutMe} />
          <Route path="/gitlab" component={GitLab} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
