import React, { Component } from "react";
import { createMuiTheme, Fab } from "@material-ui/core";
import { MuiThemeProvider } from "@material-ui/core";
import Brightness4Icon from "@material-ui/icons/Brightness4";
import { connect } from "react-redux";
import actions from "../../redux/actions";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
export class Button extends Component<any, any> {
  theme = createMuiTheme({
    palette: {
      secondary: {
        main: "#131313",
      },
    },
  });
  darkMode=()=>{
    var element = document.body;
    element.classList.toggle("darkMode");
  }

  onChange = () => {
    this.props.themeChange();
  };
  render() {
    return (
      <div className="nonor">
        <MuiThemeProvider theme={this.theme}>
          <Fab
            className="fab"
            onClick={this.darkMode}
            color="secondary"
            aria-label="Show Books"
          >
            <Brightness4Icon />
          </Fab>
        </MuiThemeProvider>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => ({
  themee: state.theme,
});

const mapDispatchToProps = (dispatch: any) => ({
  themeChange: () => dispatch(actions.changeTheme()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Button);
