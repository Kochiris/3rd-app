import React, { Component, useState } from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { Link } from "react-router-dom";

import "./NavBar.style.css";
import { IconContext } from "react-icons";
import Brightness4Icon from "@material-ui/icons/Brightness4";
import Fab from "@material-ui/core/Fab";
import Button from "./Button/Button";
import actions from "../redux/actions";
import { connect } from "react-redux";
export class Navbar extends Component<any, any> {
  showSidebar = () => {
    this.props.openSidebar();
  };
  render() {
    return (
      <>
        <IconContext.Provider value={{ color: "#fff" }}>
          <div className="navbar">
            <Link to="#" className="menu-bars">
              <FaIcons.FaBars onClick={this.showSidebar} />
            </Link>
          </div>
          <nav
            className={
              this.props.themee.sidebar ? "nav-menu active" : "nav-menu"
            }
          >
            <ul className="nav-menu-items" onClick={this.showSidebar}>
              <li className="navbar-toggle">
                <Link to="#" className="menu-bars">
                  <AiIcons.AiOutlineClose />
                </Link>
              </li>
              {this.props.themee.SidebarData.map((item: any, index: number) => {
                return (
                  <li key={index} className={item.cName}>
                    <Link to={item.path}>
                      {item.icon}
                      <span className="span">{item.title} </span>
                    </Link>
                  </li>
                );
              })}
              <li className="nav-button">
                <Button />
              </li>
            </ul>
          </nav>
        </IconContext.Provider>
      </>
    );
  }
}
const mapStateToProps = (state: any) => ({
  themee: state.theme,
});

const mapDispatchToProps = (dispatch: any) => ({
  openSidebar: () => dispatch(actions.openSidebar()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
