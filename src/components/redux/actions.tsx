import types from "./types";

const changeTheme = () => ({
  type: types.CHANGE,
});
const openSidebar = () => ({
  type: types.SIDEBAR,
});

// eslint-disable-next-line
export default {
  changeTheme,
  openSidebar,
};
