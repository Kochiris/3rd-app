import types from "./types";
import React from "react";
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import * as IoIcons from "react-icons/io";

const Redux = {
  darkMode: false,
  sidebar: false,
  SidebarData: [
    {
      title: "Home",
      path: "/",
      icon: <AiIcons.AiFillHome />,
      cName: "nav-text",
    },
    {
      title: "About Me",
      path: "/aboutme",
      icon: <IoIcons.IoIosPaper />,
      cName: "nav-text",
    },
    {
      title: "GitLab",
      path: "/gitlab",
      icon: <FaIcons.FaGitlab />,
      cName: "nav-text",
    },
  ],
};

function themeReducer(state = Redux, action: any) {
  switch (action.type) {
    case types.CHANGE:
      return {
        ...state,
        darkMode: !state.darkMode,
      };
    case types.SIDEBAR:
      return {
        ...state,
        sidebar: !state.sidebar,
      };
    default:
      return state;
  }
}

export default themeReducer;
